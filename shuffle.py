import random
import numpy as np
import scipy
from scipy import special
from math import *
import matplotlib.pyplot as plt
from sympy.combinatorics import Permutation
import itertools

## Fisher-Yates shuffle = standard computer shuffle
## but I can also used built-in pyhton shuffle:
## random.shuffle(list)

def FisherYates(n, starting_deck=False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    shuffled_deck = []
    r = n
    while r > 0:
        k = random.randint(0, r-1)
        card = starting_deck.pop(k)
        shuffled_deck.append(card)
        r -= 1
    return shuffled_deck

## riffle shuffle (generalised)

def riffle(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    l = np.random.binomial(n, 1/2) # get random cut, which is binomialy distributed
    one = starting_deck[:l]
    two = starting_deck[l:]
    shuffled = []
    while len(shuffled) < n:
        x = len(one)
        y = len(two)
        fromWhere = np.random.binomial(1, x/(x+y)) # if 1, take a card from pile one; if 0, take it from pile two
        if fromWhere == 1: 
            changing = one
        else:
            changing = two
        shuffled.append(changing.pop(0))
    return shuffled

def riffle2(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    origin = [random.random() for i in range(n)]
    origin.sort()
    originCards = list(zip(origin, starting_deck))
    #print(originCards)
    shuffled = list(map(lambda x: (2*x[0] % 1, x[1]), originCards))
    shuffled.sort()
    #print(shuffled)
    return [x[1] for x in shuffled]
    
def riffleIter(n, k):
    deck = list(range(1, n+1))
    for i in range(k):
        deck = riffle(n, deck)
    return deck

def riffle2Iter(n, k):
    deck = list(range(1, n+1))
    for i in range(k):
        deck = riffle2(n, deck)
        print(i, deck)
    return deck

## overhand shuffle

def overhandUni(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    k = random.randint(0, n)
    return starting_deck[k:] + starting_deck[:k]

def overhandBin(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    k = np.random.binomial(n, 1/2)
    return starting_deck[k+1:] + starting_deck[:k+1]

## double overhand shuffle

def doubleOverhand(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    k = random.randint(2, n-1)
    l = random.randint(1, k-1)
    one = starting_deck[:l]
    two = starting_deck[l:k]
    three = starting_deck[k:]
    #print(k, l)
    #print(one, two, three)
    return one + three + two

## triple overhand shuffle

def tripleOverhand(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1, n+1))
    k = random.randint(3, n-1)
    l = random.randint(2, k-1)
    m = random.randint(1, l-1)
    one = starting_deck[:m]
    two = starting_deck[m:l]
    three = starting_deck[l:k]
    four = starting_deck[k:]
    #print(k, l, m)
    #print(one, two, three, four)
    return two + four + three + one

## mexican spiral shuffle

def mexicanSpiral(n, starting_deck = False):
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    top = True
    while starting_deck != []:
        card = starting_deck.pop(0)
        if top:
            shuffled.append(card)
        else:
            shuffled.insert(0, card)
        top = not top
    return shuffled

## hindu shuffle

def hinduUni(n, starting_deck=False):
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = random.randint(0, len(starting_deck))
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
    return shuffled

def hinduPois(n, starting_deck=False):
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        # k must not be equal to len(deck), but can be 0
        k = min(np.random.poisson(3*(len(starting_deck)-1)//4, 1)[0], len(starting_deck)-1)
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduPoisVariable(n, cut, starting_deck=False):
    """Cut the deck expectedly at cut of the local deck, so Poisson(lambda=cut)."""
    # cut should be between 0 and 1
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = min(np.random.poisson(cut*len(starting_deck), 1)[0], len(starting_deck))
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduPoisVariableOld(n, cut, starting_deck=False):
    """Cut the deck expectedly at cut of the local deck, so Poisson(lambda=cut)."""
    # cut should be between 0 and 1
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = min(np.random.poisson(cut*(len(starting_deck)-1), 1)[0], len(starting_deck)-1)
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduBinVariableLocal(n, cut, starting_deck=False):
    """Cut the deck expectedly at cut of the local deck, so Poisson(lambda=cut)."""
    # cut should be between 0 and 1
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = np.random.binomial(len(starting_deck), cut)
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduBinVariable(n, cut, starting_deck=False):
    """Always put down a portion of the starting deck, so Binomial(n, cut), but take care - here we are cutting from the other side!!!"""
    # cut should be between 0 and 1
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = np.random.binomial(n, cut)
        if k >= len(starting_deck):
            k = 0
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduPoisVariableNotLocal(n, cut, starting_deck=False):
    """Cut the deck expectedly at cut of the initial deck, so Poisson(lambda=cut)."""
    # cut should be between 0 and 1
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = np.random.poisson(cut*n)
        if k >= len(starting_deck):
            k = 0
        shuffled += starting_deck[k:]
        starting_deck = starting_deck[:k]
        #print(k, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def hinduPoisVariableBack(n, cut, starting_deck=False):
    """Cut the deck expectedly at cut of the local deck, so Poisson(lambda=cut)."""
    # cut should be between 0 and 1
    # reversed viewpoint on cut!!!
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    shuffled = []
    while len(starting_deck) > 0:
        k = min(np.random.poisson(cut*len(starting_deck), 1)[0], len(starting_deck))
        k2 = len(starting_deck) - k
        
        shuffled += starting_deck[k2:]
        starting_deck = starting_deck[:k2]
        #print(k, k2, "shuffled: ", shuffled, "else: ", starting_deck)
    return shuffled

def reversedShuffle(n, starting_deck=False):
    if not starting_deck:
        starting_deck = list(range(1,n+1))
    return starting_deck[::-1]

#print(reversedShuffle(5))

## total variation distance for riffle shuffle (theoretical)
    
def memo(fn):
    cache = {}
    miss = object()
    
    def wrapper(*args):
        result = cache.get(args, miss)
        if result is miss:
            result = fn(*args)
            cache[args] = result
        return result
        
    return wrapper
 
@memo 
def A(n, r):
    if r == 1:
        return 1
    if r > (n+1)/2:
        return A(n, n-r+1)
    return r**n - sum([scipy.special.binom(n + r - j, n) * A(n, j) for j in range(1, r)])

#print(A(1, 0), A(2, 0), A(2, 1), A(3, 1))
#print([A(52, r) for r in range(1, 52+1)])

    
def TVD(n, k):
    return 1/2 * sum([A(n, r) * abs(scipy.special.binom(2**k + n - r, n)/(2 **(n * k)) - 1/(factorial(n))) for r in range(1, n+1)])

#print([TVD(52, k) for k in range(0, 17)])
meja7 = TVD(52, 7)

## exact TVD for overhand shuffle

def TVDoverhandUni(n, k):
    '''n cards, k overhand uniform shuffles'''
    Pu = 1/factorial(n)
    return 1/2 * (abs(2/(n+1) - Pu) + (n-1) * abs(1/(n+1) - Pu) + (factorial(n) - n)*Pu)

#print(TVDoverhandUni(10, 5), TVDoverhandUni(10, 10), TVDoverhandUni(10, 15), TVDoverhandUni(10, 20))

#x = list(range(0, 31))
#y = [TVDoverhandUni(52, k) for k in range(0, 31)]
#meja7 = TVD(52, 7)
#plt.plot(x, y, 'o', x, [meja7 for i in range(0, 31)])
#plt.xlabel('Number of iterations')
#plt.ylabel('Exact TVD measure')
#plt.title('Overhand uniform shuffle')
#plt.show()

@memo
def inner_sum(n, k, t):
    if k == 1:
        return scipy.special.binom(n, t)        
    suma = 0
    for s1 in range(0, n+1):
        suma += scipy.special.binom(n, s1) * inner_sum(n, k-1, t-s1 % n)
    return suma

def TVDoverhandBin(n, k):
    '''n cards, k overhand binomial shuffles'''
    Pu = 1/factorial(n)
    partial = sum([abs((1/2)**(n*k) * inner_sum(n, k, t) - Pu) for t in range(0, n+1)])
    return 1/2 * (partial + (factorial(n) - n)*Pu)

#x = list(range(1, 11))
#y = [TVDoverhandBin(52, k) for k in range(1, 11)]
#meja7 = TVD(52, 7)
#plt.plot(x, y, 'o', x, [meja7 for i in range(1, 11)])
#plt.xlabel('Number of iterations')
#plt.ylabel('Exact TVD measure')
#plt.title('Overhand binomial shuffle')
#plt.show()

#print(TVDoverhandBin(5, 5), TVDoverhandBin(5, 10), TVDoverhandBin(5, 15), TVDoverhandBin(5, 20))
#print(TVDoverhandBin(10, 5), TVDoverhandBin(10, 10), TVDoverhandBin(10, 15), TVDoverhandBin(10, 20))
#print(TVDoverhandBin(20, 5), TVDoverhandBin(20, 10), TVDoverhandBin(20, 15), TVDoverhandBin(20, 20))

## measure: TVD for small number of cards

def TVDapprox(n, shuffle, k, iterations, draw = True):
    '''Draw histogram. n cards, use shuffle k times. Do iterations.'''
    permutations = list(itertools.permutations(range(1, n+1)))
    freq = {p:0 for p in permutations}
    for i in range(iterations):
        deck = list(range(1, n+1))
        for l in range(k):
            deck = shuffle(n, deck)
        freq[tuple(deck)] += 1/iterations
    #print(freq)
    if draw:
        pos = np.arange(len(permutations))
        width = 1.0     # gives histogram aspect to the bar diagram

        ax = plt.axes()
        ax.set_xticks(pos + (width / 2))
        ax.set_xticklabels(['1' if p == tuple(range(1, n+1)) else ' ' for p in freq.keys()])

        plt.bar(pos, freq.values(), width, color='g')
        plt.show()
    
    Pu = 1/factorial(n)
    return 1/2 * sum([abs(freq[p] - Pu) for p in permutations])

#print(TVDapprox(6, riffle, 7, 2000))
#print([TVDapprox(6, riffle, i, 3000, False) for i in range(1, 11)])
#print([TVDapprox(6, overhandUni, i, 3000, False) for i in range(1, 11)])
#print([TVDapprox(6, overhandBin, i, 3000, False) for i in range(1, 11)])
#TVDapprox(5, overhandBin, 5, 2000)
#TVDapprox(5, overhandBin, 7, 2000)
#TVDapprox(5, overhandBin, 9, 2000)
#TVDapprox(5, overhandBin, 11, 2000)
#TVDapprox(5, overhandBin, 13, 2000)
#TVDapprox(5, overhandUni, 7, 10000)
#print(TVDapprox(6, doubleOverhand, 7, 1000))
#print(TVDapprox(6, tripleOverhand, 7, 1000))
#print(TVDapprox(6, mexicanSpiral, 19, 1000, False))

## drawing function

def risiMeasureIterations(shuffle, measure, kMax, n):
    results = measure(shuffle, n, kMax)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'o')
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel(measure.__name__)
    plt.title(shuffle.__name__ + ' shuffle')
    #plt.show()
    ime = shuffle.__name__ + '_' + measure.__name__ + '_' + str(kMax) + '.jpg'
    plt.savefig(ime)
    print("Saved " + ime)
    plt.clf()
    return None

def izpis(shuffle, measure, kMax, n):
    results = measure(shuffle, n, kMax)
    x = [0] + list(results.keys())
    y = [(1, 0)] + list(results.values())
    for i in range(len(x)):
        print('{0:<3d} {1:.4f} +- {2:.4f}'.format(x[i], y[i][0], y[i][1]))
    return None

def izpisVse(shuffle, measures, kMax, n):
    niz = '   '.join(['{0:^16}'.format(m.__name__) for m in measures])
    print('    ' + niz)
    izpis = ['{0:<3d} '.format(i) for i in range(kMax+1)]
    for measure in measures:
        results = measure(shuffle, n, kMax)
        y = [1] + list(results.values())
        for i in range(kMax+1):
            izpis[i] += '{0:^16.4f}   '.format(y[i])
    print('\n'.join(izpis))
    return None
        
def mu(sample, real):
    '''This is not expected value, but TVD.'''
    return 1/2 * sum(map(lambda x: abs(x), sample - real))
def E(sample):
    return sum([(i+1) * sample[i] for i in range(len(sample))])
def sigma2(sample):
    m = E(sample)
    return sqrt(sum([sample[i] * (i+1)**2 for i in range(len(sample))]) - m**2)
    
    
## measure: distribution of location of one fixed card (should be uniform)

def oneCardMeasure(shuffle, n, kMax, card = 1, iterations = 1000):
    results = {i:[0 for j in range(n)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            results[k][deck.index(card)] += 1/iterations
    #print(results)
    realRandom = np.array([1/n for j in range(n)])
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}
        
#risiMeasureIterations(riffle, oneCardMeasure, 12, 52)
#risiMeasureIterations(riffle2, oneCardMeasure, 12, 52)
#risiMeasureIterations(overhandUni, oneCardMeasureNew, 12, 52)
#risiMeasureIterations(overhandBin, oneCardMeasureNew, 12, 52)
#risiMeasureIterations(doubleOverhand, oneCardMeasureNew, 12, 52)
#risiMeasureIterations(tripleOverhand, oneCardMeasureNew, 12, 52)
#risiMeasureIterations(mexicanSpiral, oneCardMeasureNew, 12, 52)

        
#print(oneCardMeasure(riffle, 10, 9))
#print(oneCardMeasure(riffle, 52, 9))
#print(oneCardMeasure(overhandUni, 52, 20))
#print(oneCardMeasure(overhandUni, 52, 20, 15))
#print(oneCardMeasure(overhandBin, 52, 20))
#print(oneCardMeasure(overhandBin, 52, 20, 15))

## measure: distribution of distance between two cards: if shuffling is truly random, location of each card is uniformly distributed, so p(|X−Y|=s)=2(N−s) and E|X−Y|=(N^2−1)/(3N) where N is the number of cards

def twoCardsMeasure(shuffle, n, kMax, card1 = 1, card2 = 2, iterations = 2000):
    results = {i:[0 for j in range(0, n)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            results[k][abs(deck.index(card1) - deck.index(card2))] += 1/iterations
    realRandom = np.array([2 * (n - s) / (n**2) for s in range(0, n)])
    #print(realRandom)
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

#risiMeasureIterations(riffle, twoCardsMeasureNew, 12, 52)
#risiMeasureIterations(overhandUni, twoCardsMeasureNew, 12, 52)
#risiMeasureIterations(overhandBin, twoCardsMeasureNew, 12, 52)
#risiMeasureIterations(doubleOverhand, twoCardsMeasureNew, 12, 52)
#risiMeasureIterations(tripleOverhand, twoCardsMeasureNew, 12, 52)
#risiMeasureIterations(mexicanSpiral, twoCardsMeasureNew, 12, 52)
    
#print(twoCardsMeasure(riffle, 52, 9))
#print(twoCardsMeasure(riffle, 52, 9, 1, 15))
#print(twoCardsMeasure(riffle, 52, 9, 1, 50))
#print(twoCardsMeasure(overhandUni, 52, 9))
#print(twoCardsMeasure(overhandUni, 52, 9, 1, 15))
#print(twoCardsMeasure(overhandUni, 52, 9, 1, 50))
#print(twoCardsMeasure(overhandBin, 52, 9))
#print(twoCardsMeasure(overhandBin, 52, 9, 1, 15))
#print(twoCardsMeasure(overhandBin, 52, 9, 1, 50))

## rising sequences measure

def number_rising(p):
    '''Number of rising sequences in permutation p, represented as a list with elements 1, 2, ..., n.'''
    number = 0
    n = len(p)
    t = 1
    while t < n:
        i = p.index(t)
        j = p.index(t+1)
        if i < j:
            t = t+1
        else:
            t = t+1
            number += 1
    return number+1

def risingMeasure(shuffle, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def risingMeasureHinduPoissVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def risingMeasureHinduBinVarLoc(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariableLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def risingMeasureHinduBinVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def risingMeasureHinduPoissVarNotLocal(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableNotLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def risingMeasureHinduPoissVarBack(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableBack(n, cut, deck)
            #print(it, k, deck)
            results[k][number_rising(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array([A(n, r)/fn for r in range(0, n+1)])
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

#risiMeasureIterations(riffle, risingMeasure, 12, 52)
#risiMeasureIterations(overhandUni, risingMeasure, 12, 52)
#risiMeasureIterations(overhandBin, risingMeasure, 12, 52)
#risiMeasureIterations(doubleOverhand, risingMeasure, 12, 52)
#risiMeasureIterations(tripleOverhand, risingMeasure, 12, 52)
#risiMeasureIterations(mexicanSpiral, risingMeasure, 12, 52)

#print(risingMeasureNew(overhandUni, 6, 6, 5))

#print(risingMeasure(riffle, 52, 10))
#print(risingMeasure(overhandUni, 52, 10))
#print(risingMeasure(overhandBin, 52, 10))

#print(risingMeasureNew(riffle, 52, 10))
#print(risingMeasureNew(overhandUni, 52, 10))
#print(risingMeasureNew(overhandBin, 52, 10))

def number_suitsChanges(p):
    """Number of suits changes in deck/permutation p. WLOG suits can be represented as cards with remainder 0, 1, 2, 3 modulo 4."""
    trans = list(map(lambda x: x % 4, p))
    changes = 0
    last = trans[0]
    for t in trans:
        if t != last:
            changes += 1
            last = t
    return changes

def real_number_suitsChanges(n, iterations = 10000):
    """Take deck of n cards. Randomly shuffle it with a computer. Count number of suits changes. Return a vector of this distribution."""
    results = [0 for j in range(0, n+1)]
    for it in range(iterations):
        deck = list(range(1, n+1))
        random.shuffle(deck)
        #print(it, k, deck)
        results[number_suitsChanges(deck)] += 1/iterations
    return results


def suitsChangesMeasure(shuffle, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def suitsChangesMeasureHinduPoissVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def suitsChangesMeasureHinduBinVarLoc(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariableLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}


def suitsChangesMeasureHinduBinVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def suitsChangesMeasureHinduPoissVarNotLocal(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableNotLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def suitsChangesMeasureHinduPoissVarBack(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableBack(n, cut, deck)
            #print(it, k, deck)
            results[k][number_suitsChanges(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_number_suitsChanges(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def length_identical(p):
    """Returns maximal length of subsequence in p that consists of i, i+1, ..., i+k or i, i-1, ..., i-k. So initial deck is 1, 2, ..., n."""
    upLen = 1
    upLast = p[0]
    downLen = 1
    downLast = p[0]
    for x in p[1:]:
        if x == upLast+1:
            upLen += 1
            upLast = x
        elif x == downLast - 1:
            downLen += 1
            downLast = x
    return max(upLen, downLen)

def real_identicalLength(n, iterations = 10000):
    """Take deck of n cards. Randomly shuffle it with a computer. Count lengths of longest identical subsequences. Return a vector of this distribution."""
    results = [0 for j in range(0, n+1)]
    for it in range(iterations):
        deck = list(range(1, n+1))
        random.shuffle(deck)
        #print(it, k, deck)
        results[length_identical(deck)] += 1/iterations
    return results

def longestLengthMeasure(shuffle, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def longestLengthMeasureHinduPoissVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def longestLengthMeasureHinduBinVarLoc(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariableLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def longestLengthMeasureHinduBinVar(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariable(n, cut, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def longestLengthMeasureHinduPoissVarNotLocal(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableNotLocal(n, cut, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}

def longestLengthMeasureHinduPoissVarBack(cut, n, kMax, iterations = 1000):
    results = {i:[0 for j in range(0, n+1)] for i in range(1, kMax+1)}
    for it in range(iterations):
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableBack(n, cut, deck)
            #print(it, k, deck)
            results[k][length_identical(deck)] += 1/iterations
    fn = factorial(n)
    realRandom = np.array(real_identicalLength(n))
    #print(realRandom, sum(realRandom))
    #print(results)
    return {i : mu(results[i],realRandom) for i in range(1, kMax+1)}
        
# patience game

def patienceGame(deck):
    '''Returns true, if we win the game. Else, returns false.'''
    n = len(deck)
    border = n//2
    Yin = []
    Yang = []
    third = []
    finishedYin = border
    finishedYang = n - border
    while len(Yin)!= finishedYin and len(Yang) != finishedYang:
        #print(deck, Yin, Yang)
        for c in deck:
            #print(c)
            if c <= border:
                if len(Yin) == 0 and c == 1:
                    Yin.append(c)
                elif len(Yin) != 0 and Yin[-1] == c-1:
                    Yin.append(c)
                else:
                    third.append(c)
            elif c > border:
                if len(Yang) == 0 and c == n:
                    Yang.append(c)
                elif len(Yang) != 0 and Yang[-1] == c+1:
                    Yang.append(c)
                else:
                    third.append(c)
            if len(Yin) == finishedYin or len(Yang) == finishedYang:
                break
        deck = third
        third = []
    if len(Yin) == finishedYin:
        return True
    else:
        return False
    
def patienceMeasure(shuffle, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = shuffle(n, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}

def patienceMeasureHinduBinVariable(cut, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariable(n, cut, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}

def patienceMeasureHinduPoisVariable(cut, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariable(n, cut, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}

def patienceMeasureHinduBinVariableLocal(cut, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduBinVariableLocal(n, cut, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}

def patienceMeasureHinduPoisVariableNotLocal(cut, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableNotLocal(n, cut, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}

def patienceMeasureHinduPoisVariableBack(cut, n, kMax, iterations = 1000):
    results = {i:[0] for i in range(1, kMax+1)}
    realRandom = 0
    for it in range(iterations):
        realDeck = list(range(1, n+1))
        random.shuffle(realDeck)
        if patienceGame(realDeck):
            realRandom += 1/iterations        
        deck = list(range(1, n+1))
        for k in range(1, kMax+1):
            deck = hinduPoisVariableBack(n, cut, deck)
            if patienceGame(deck):
                results[k][0] += 1/iterations
    realRandom = np.array([realRandom])
    #print(realRandom)
    return realRandom[0], {i : results[i] for i in range(1, kMax+1)}


def risiPatience(n, kMax, paramPoisson, paramBin, paramBinLocal, paramPoissonNotLocal, paramBack):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Patience game measure')
    
    for cut in paramPoisson:
        results = patienceMeasureHinduPoisVariable(cut, n, kMax, 200)[1]
        x = [0] + list(results.keys())
        y = [1] + list([x[0] for x in results.values()])
        plt.plot(x, y, '-', label=str(cut) + " poisson local")
        
    for cut in paramBinLocal:
        results = patienceMeasureHinduBinVariableLocal(cut, n, kMax, 200)[1]
        x = [0] + list(results.keys())
        y = [1] + list([x[0] for x in results.values()])
        plt.plot(x, y, '-', label=str(cut) + " bin local")
        
    
    for cut in paramBin:
        results = patienceMeasureHinduBinVariable(cut, n, kMax, 200)[1]
        x = [0] + list(results.keys())
        y = [1] + list([x[0] for x in results.values()])
        plt.plot(x, y, '-', label=str(cut) + " binomial")
    
    for cut in paramPoissonNotLocal:
        results = patienceMeasureHinduPoisVariableNotLocal(cut, n, kMax, 200)[1]
        x = [0] + list(results.keys())
        y = [1] + list([x[0] for x in results.values()])
        plt.plot(x, y, '-', label=str(cut) + " poisson")
        
    for cut in paramBack:
        results = patienceMeasureHinduPoisVariableBack(cut, n, kMax, 200)[1]
        x = [0] + list(results.keys())
        y = [1] + list([x[0] for x in results.values()])
        plt.plot(x, y, '-', label=str(cut) + " poisson local back")
    
    # border
    plt.plot([-0.2, kMax+0.2], [0.5, 0.5], 'orange')

    
    ime = "patience_hindu_variations" + str(n) + 'cards' + '_' + str(kMax) + '_200' + 'iter_' + str(len(paramPoisson)) + '_' + str(len(paramBinLocal)) + '_' + str(len(paramBin)) + '_' + str(len(paramBack)) + '_param.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiPatience(52, 100, [0.9], [0.9], [0.9], [0.9], [0.1])        
#risiPatience(52, 100, [0.9, 0.99], [], [0.9, 0.99], [], [0.1, 0.01])  
#risiPatience(52, 100, [0.9, 0.95, 0.99], [], [], [], [])   
        
# drawing for riffle shuffle

def risiRiffle(n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Riffle shuffle - all measures')
    
    # one card measure
    results = oneCardMeasure(riffle, n, kMax, 1, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'ro-', label="one card measure for the 1st card")
    
    # two cards measure
    results = twoCardsMeasure(riffle, n, kMax, 1, 2, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'b^-', label="two cards measure for the 1st and 2nd card")
    
    # rising sequences measure
    results = risingMeasure(riffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'gs-', label="rising sequences measure")
    
    # suits changes measure
    results = suitsChangesMeasure(riffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'mv-', label="suits changes measure")
    
    # longest identical length measure
    results = longestLengthMeasure(riffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'y3-', label="longest length measure")
    
    # exact TVD
    x = [i for i in range(0, kMax+1)]
    y = [TVD(n, k) for k in range(0, kMax+1)]
    plt.plot(x, y, 'c*-', label="exact total variation distance")
    
    # patience
    results = patienceMeasure(riffle, n, kMax, 1000)[1]
    x = [0] + list(results.keys())
    y = [1] + list([2*abs(x[0] - 0.5) for x in results.values()])
    plt.plot(x, y, 'k+-', label="patience game measure")
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = 'riffle_' + str(n) + 'cards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

def risiRiffle2(n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Riffle2 shuffle - all measures')
    
    # one card measure
    results = oneCardMeasure(riffle2, n, kMax, 1, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'ro-', label="one card measure for the 1st card")
    
    # two cards measure
    results = twoCardsMeasure(riffle2, n, kMax, 1, 2, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'b^-', label="two cards measure for the 1st and 2nd card")
    
    # rising sequences measure
    results = risingMeasure(riffle2, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'gs-', label="rising sequences measure")
    
    # suits changes measure
    results = suitsChangesMeasure(riffle2, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'mv-', label="suits changes measure")
    
    # longest identical length measure
    results = longestLengthMeasure(riffle2, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'y3-', label="longest length measure")
    
    # exact TVD
    x = [i for i in range(0, kMax+1)]
    y = [TVD(n, k) for k in range(0, kMax+1)]
    plt.plot(x, y, 'c*-', label="exact total variation distance")
        
    # patience
    results = patienceMeasure(riffle2, n, kMax, 1000)[1]
    x = [0] + list(results.keys())
    y = [1] + list([2*abs(x[0] - 0.5) for x in results.values()])
    plt.plot(x, y, 'k+-', label="patience game measure")
    
    # 10, 5, 1 %  and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = 'riffle2_' + str(n) + 'cards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

def risiOverhandUni(n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Uniform overhand shuffle - all measures')
    
    # one card measure
    results = oneCardMeasure(overhandUni, n, kMax, 1, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'ro-', label="one card measure for the 1st card")
    
    # two cards measure
    results = twoCardsMeasure(overhandUni, n, kMax, 1, 2, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'b^-', label="two cards measure for the 1st and 2nd card")
    
    # rising sequences measure
    results = risingMeasure(overhandUni, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'gs-', label="rising sequences measure")
    
    # suits changes measure
    results = suitsChangesMeasure(overhandUni, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'mv-', label="suits changes measure")
    
    # longest identical length measure
    results = longestLengthMeasure(overhandUni, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'y3-', label="longest length measure")
    
    # exact TVD
    x = [i for i in range(0, kMax+1)]
    y = [TVDoverhandUni(n, k) for k in range(0, kMax+1)]
    plt.plot(x, y, 'c*-', label="exact total variation distance")    
    
    # patience
    results = patienceMeasure(overhandUni, n, kMax, 1000)[1]
    x = [0] + list(results.keys())
    y = [1] + list([2*abs(x[0] - 0.5) for x in results.values()])
    plt.plot(x, y, 'k+-', label="patience game measure")
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = 'overhandUni_' + str(n) + 'cards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

def risiOverhandBin(n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Binomial overhand shuffle - all measures')
    
    # one card measure
    results = oneCardMeasure(overhandBin, n, kMax, 1, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'ro-', label="one card measure for the 1st card")
    
    # two cards measure
    results = twoCardsMeasure(overhandBin, n, kMax, 1, 2, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'b^-', label="two cards measure for the 1st and 2nd card")
    
    # rising sequences measure
    results = risingMeasure(overhandBin, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'gs-', label="rising sequences measure")
    
    # suits changes measure
    results = suitsChangesMeasure(overhandUni, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'mv-', label="suits changes measure")
    
    # longest identical length measure
    results = longestLengthMeasure(overhandBin, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'y3-', label="longest length measure")
    
    # exact TVD
    x = [i for i in range(0, kMax+1)]
    y = [1] + [TVDoverhandBin(n, k) for k in range(1, kMax+1)]
    plt.plot(x, y, 'c*-', label="exact total variation distance")
        
    # patience
    results = patienceMeasure(overhandBin, n, kMax, 1000)[1]
    x = [0] + list(results.keys())
    y = [1] + list([2*abs(x[0] - 0.5) for x in results.values()])
    plt.plot(x, y, 'k+-', label="patience game measure")
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = 'overhandBin_' + str(n) + 'cards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

def risiOther(shuffle, n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title(shuffle.__name__ + 'shuffle - all measures')
    
    # one card measure
    results = oneCardMeasure(shuffle, n, kMax, 1, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'ro-', label="one card measure for the 1st card")
    
    # two cards measure
    results = twoCardsMeasure(shuffle, n, kMax, 1, 2, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'b^-', label="two cards measure for the 1st and 2nd card")
    
    # rising sequences measure
    results = risingMeasure(shuffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'gs-', label="rising sequences measure")
    
    # suits changes measure
    results = suitsChangesMeasure(shuffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'mv-', label="suits changes measure")
    
    # longest identical length measure
    results = longestLengthMeasure(shuffle, n, kMax, 1000)
    x = [0] + list(results.keys())
    y = [1] + list(results.values())
    plt.plot(x, y, 'y3-', label="longest length measure")
    
    # patience
    results = patienceMeasure(shuffle, n, kMax, 1000)[1]
    x = [0] + list(results.keys())
    y = [1] + list([2*abs(x[0] - 0.5) for x in results.values()])
    plt.plot(x, y, 'k+-', label="patience game measure")
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = shuffle.__name__ + str(n) + 'cards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiRiffle(52, 16)
#risiRiffle2(52, 16)
#risiOverhandUni(52, 16)
#risiOverhandBin(52, 16)
#risiOther(doubleOverhand, 52, 16)
#risiOther(tripleOverhand, 52, 16)
#risiOther(mexicanSpiral, 52, 16)
#risiOther(hinduUni, 52, 16)
#risiOther(hinduPois, 52, 16)
#risiOther(FisherYates, 52, 16)
#risiOther(reversedShuffle, 52, 16)

def risiPoisson(n, kMax, paramPoisson, paramBin, paramBinLocal, paramPoissonNotLocal, paramBack):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('hindu shuffle - different parameters')
    
    for p in paramPoisson:
        results = risingMeasureHinduPoissVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label=str(p) + " poisson local, rising measure")
        plt.text(2, y[2], str(p))
        
        results = suitsChangesMeasureHinduPoissVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 's-', label=str(p) + " poisson local, suits changes measure")
        plt.text(3, y[3], str(p))
        
        results = longestLengthMeasureHinduPoissVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '3-', label=str(p) + " poisson local, longest length measure")
        plt.text(4, y[4], str(p))
        
    for p in paramBinLocal:
        results = risingMeasureHinduBinVarLoc(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label=str(p) + " bin local")
        plt.text(2, y[2], str(p))
        
        results = suitsChangesMeasureHinduBinVarLoc(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 's-', label=str(p) + " bin local")
        plt.text(3, y[3], str(p))
        
        results = longestLengthMeasureHinduBinVarLoc(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '3-', label=str(p) + " bin local")
        plt.text(4, y[4], str(p))
    
    for p in paramBin:
        results = risingMeasureHinduBinVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '^-', label=str(p) + " binomial")
        plt.text(2, y[2], str(p))
        
        results = suitsChangesMeasureHinduBinVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '*-', label=str(p) + " binomial")
        plt.text(3, y[3], str(p))
        
        results = longestLengthMeasureHinduBinVar(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '+-', label=str(p) + " binomial")
        plt.text(4, y[4], str(p))
        
    for p in paramPoissonNotLocal:
        results = risingMeasureHinduPoissVarNotLocal(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label=str(p) + " poisson")
        plt.text(2, y[2], str(p))
        
        results = suitsChangesMeasureHinduPoissVarNotLocal(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 's-', label=str(p) + " poisson")
        plt.text(3, y[3], str(p))
        
        results = longestLengthMeasureHinduPoissVarNotLocal(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '3-', label=str(p) + " poisson")
        plt.text(4, y[4], str(p))
        
    for p in paramBack:
        results = risingMeasureHinduPoissVarBack(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label=str(p) + " poisson local back, rising measure")
        plt.text(2, y[2], str(p))
        
        results = suitsChangesMeasureHinduPoissVarBack(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 's-', label=str(p) + " poisson local back, suits changes measure")
        plt.text(3, y[3], str(p))
        
        results = longestLengthMeasureHinduPoissVarBack(p, n, kMax, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, '3-', label=str(p) + " poisson local back, longest length measure")
        plt.text(4, y[4], str(p))
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = "hindu_variations" + str(n) + 'cards' + '_' + str(kMax) + 'iter_' + str(len(paramPoisson)) + '_' + str(len(paramBinLocal)) + '_' + str(len(paramBin)) + '_' + str(len(paramPoissonNotLocal)) + '_' + str(len(paramBack)) +  '_param.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiPoisson(52, 16, [0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
#risiPoisson(52, 16, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],[], [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [])
#risiPoisson(52, 16, [0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1], [], [], [])

#risiPoisson(52, 16, [0.9, 0.95, 1], [], [], [])
#risiPoisson(52, 16, [], [], [0.9, 0.95], [])

#risiPoisson(52, 16, [0.9, 0.95, 1], [], [0.9, 0.95], [])
#risiPoisson(52, 16,[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [], [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [])
#risiPoisson(52, 16, [0.5, 0.75, 0.9, 1], [], [0.5, 0.75, 0.9, 0.99], [])
#risiPoisson(52, 16, [0.75], [], [0.75], [])
#risiPoisson(52, 16, [], [], [0.97, 0.971, 0.972, 0.973, 0.974, 0.975, 0.976, 0.977, 0.978, 0.979, 0.98], [])
#risiPoisson(52, 16, [], [], [0.97, 0.971, 0.972, 0.973, 0.974], [])

#risiPoisson(52, 16, [1], [], [], [])

def izpisVseVarBin(p, kMax, n):
    """For local binomial."""
    niz = '   '.join(['{0:^16}'.format(m) for m in ["rising", "suitsChanges", "longestLength"]])
    print('    ' + niz)
    izpis = ['{0:<3d} '.format(i) for i in range(kMax+1)]
    for measure in [risingMeasureHinduBinVarLoc, suitsChangesMeasureHinduBinVarLoc, longestLengthMeasureHinduBinVarLoc]:
        results = measure(p, n, kMax, 1000)
        y = [1] + list(results.values())
        for i in range(kMax+1):
            izpis[i] += '{0:^16.4f}   '.format(y[i])
    print('\n'.join(izpis))
    return None

def izpisVseVarPois(p, kMax, n):
    """For local poisson."""
    niz = '   '.join(['{0:^16}'.format(m) for m in ["rising", "suitsChanges", "longestLength"]])
    print('    ' + niz)
    izpis = ['{0:<3d} '.format(i) for i in range(kMax+1)]
    for measure in [risingMeasureHinduPoissVar, suitsChangesMeasureHinduPoissVar, longestLengthMeasureHinduPoissVar]:
        results = measure(p, n, kMax, 1000)
        y = [1] + list(results.values())
        for i in range(kMax+1):
            izpis[i] += '{0:^16.4f}   '.format(y[i])
    print('\n'.join(izpis))
    return None

def risiDifferentCards1(shuffle, n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title(shuffle.__name__ + 'shuffle - observing different cards')
    maksi = {i:[0, 0] for i in range(1, kMax+1)}
    mini = {i:[1, 0] for i in range(1, kMax+1)}
    for i in range(1, n+1):
        results = oneCardMeasure(shuffle, n, kMax, i, 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label=str(i))
        for j in range(1, kMax+1):
            if results[j] > maksi[j][0]:
                maksi[j][0] = results[j]
                maksi[j][1] = i
            if results[j] < mini[j][0]:
                mini[j][0] = results[j]
                mini[j][1] = i
        
    ime = shuffle.__name__ + str(n) + 'cards' + '_' + str(kMax) + 'iter_differentCards.jpg'
    for j in range(1, kMax+1):
        plt.text(j, maksi[j][0] + 0.02, str(maksi[j][1]))
        plt.text(j-0.1, mini[j][0] - 0.05, str(mini[j][1]))
    lgd = plt.legend(loc='center right', bbox_to_anchor=(1.2,1))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiDifferentCards1(riffle, 52, 12)
#risiDifferentCards1(overhandUni, 52, 12)
#risiDifferentCards1(overhandBin, 52, 12)
#risiDifferentCards1(doubleOverhand, 52, 12)
#risiDifferentCards1(tripleOverhand, 52, 12)
#risiDifferentCards1(mexicanSpiral, 52, 12)
#risiDifferentCards1(hinduUni, 52, 12)
#risiDifferentCards1(hinduPois, 52, 12)
#risiDifferentCards1(FisherYates, 52, 12)

def risiDifferentCards2(shuffle, n, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title(shuffle.__name__ + 'shuffle - observing different pairs of cards')
    for pair in [[1, 2], [1,3], [1, n//2], [1, n-1], [1, n], [2, 3], [n//2, n//2 +1], [n//2, n], [n-1, n], [n//3, 2*n//3]]:
        results = twoCardsMeasure(shuffle, n, kMax, pair[0], pair[1], 1000)
        x = [0] + list(results.keys())
        y = [1] + list(results.values())
        plt.plot(x, y, 'o-', label='cards '+str(pair[0]) + ' and ' + str(pair[1]))
        
    ime = shuffle.__name__ + str(n) + 'cards' + '_' + str(kMax) + 'iter_differentCardsPairs.jpg'
    lgd = plt.legend(loc='center right', bbox_to_anchor=(1.45,0.5))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiDifferentCards2(riffle, 52, 12)
#risiDifferentCards2(overhandUni, 52, 12)
#risiDifferentCards2(overhandBin, 52, 12)
#risiDifferentCards2(doubleOverhand, 52, 12)
#risiDifferentCards2(tripleOverhand, 52, 12)
#risiDifferentCards2(mexicanSpiral, 52, 12)
#risiDifferentCards2(hinduUni, 52, 12)
#risiDifferentCards2(hinduPois, 52, 12)
#risiDifferentCards2(FisherYates, 52, 12)

# analysis of k0

def border(listed, value):
    if min(listed) > value:
        return 'inf'
    return 1 + list(map(lambda x: x <= value, listed)).index(True)

def getK0(shuffle, n, kMax):
    K0 = []
    
    # one card measure
    results = oneCardMeasure(shuffle, n, kMax, 1, 1000)
    l = [x[0] for x in results.values()]
    K0.append([shuffle.__name__, "1 card measure",border(l, meja7), border(l, 0.1), border(l, 0.05), border(l, 0.01)])
    
    # two cards measure
    results = twoCardsMeasure(shuffle, n, kMax, 1, 2, 1000)
    l = [x[0] for x in results.values()]
    K0.append([shuffle.__name__, "2 cards measure", border(l, meja7), border(l, 0.1), border(l, 0.05), border(l, 0.01)])
    
    # rising sequences measure
    results = risingMeasure(shuffle, n, kMax, 1000)
    l = [x[0] for x in results.values()]
    K0.append([shuffle.__name__, "rising measure", border(l, meja7), border(l, 0.1), border(l, 0.05), border(l, 0.01)])    
    
    return K0

def getOneK0(shuffle, measure, n, kMax, edge):
    results = measure(shuffle, n, kMax)
    l = [x[0] for x in results.values()]
    return border(l, edge)

def getAllK0(shuffle, n, kMax, edge):
    t = [('one card measure', getOneK0(shuffle, oneCardMeasure, n, kMax, 0.2)), 
         ('two cards measure', getOneK0(shuffle, twoCardsMeasure, n, kMax, 0.2)),
         ('rising sequences measure', getOneK0(shuffle, risingMeasure, n, kMax, 0.2)),
         ('suitsChangesMeasure', getOneK0(shuffle, suitsChangesMeasure, n, kMax, 0.2)),
         ('longest overlap measure', getOneK0(shuffle, longestLengthMeasure, n, kMax, 0.2))]
    if shuffle == riffle or shuffle == riffle2:
        t.append(('exact TVD', border([TVD(n, k) for k in range(1, kMax+1)], 0.2)))
    if shuffle == overhandUni:
        t.append(('exact TVD', border([TVDoverhandUni(n, k) for k in range(1, kMax+1)], 0.2)))
    if shuffle == overhandBin:
        t.append(('exact TVD', border([TVDoverhandBin(n, k) for k in range(1, kMax+1)], 0.2)))
    
    return None

#for shuffle in [riffle2, overhandUni, overhandBin, doubleOverhand, tripleOverhand, mexicanSpiral, hinduUni, hinduPois, FisherYates]:
    #print(getK0(shuffle, 10, 30))
    
def risiRiffleDifferentN(nList, kMax):
    plt.axis([-0.2, kMax+0.2, -0.01, 1.03])
    plt.xlabel('number of iterations')
    plt.ylabel('randomness')
    plt.title('Riffle shuffle - different number of cards - exact TVD measure')
    
    for n in nList:
        x = [i for i in range(0, kMax+1)]
        y = [TVD(n, k) for k in range(0, kMax+1)]
        plt.plot(x, y, 'o-', label=str(n) + ' cards')
    
    # 10, 5, 1 % and meja7 borders
    plt.plot([-0.2, kMax+0.2], [0.1, 0.1], 'orange')
    plt.plot([-0.2, kMax+0.2], [0.05, 0.05], 'darkorange')
    plt.plot([-0.2, kMax+0.2], [0.01, 0.01], 'orangered')
    plt.plot([-0.2, kMax+0.2], [meja7, meja7], 'gold')
    
    ime = 'riffle_' + 'differentNumberOfCards' + '_' + str(kMax) + 'iter.jpg'
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2))
    plt.savefig(ime, bbox_extra_artists=(lgd,), bbox_inches='tight')
    #plt.show()
    print("Saved " + ime)
    plt.clf()
    return None

#risiRiffleDifferentN([5, 10, 15, 20, 25, 30, 35, 40, 45, 50], 20)



#results 
#>>> patienceMeasure(overhandUni, 52, 10) 
#(0.50900000000000034, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(overhandBin, 52, 10)   
#(0.49900000000000039, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(doubleOverhand, 52, 10)              
#(0.51800000000000035, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(tripleOverhand, 52, 10)
#(0.47100000000000036, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(mexicanSpiral, 52, 10)
#(0.50100000000000033, {1: [0], 2: [0], 3: [1.0000000000000007], 4: [0], 5: [0], 6: [0], 7: [0], 8: [0], 9: [1.0000000000000007], 10: [0]})
#>>> patienceMeasure(hinduUni, 52, 10)
#(0.48300000000000037, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(hinduPois, 52, 10)
#(0.51000000000000034, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [0.9970000000000008], 8: [0.9930000000000008], 9: [0.9760000000000008], 10: [0.9660000000000007]})
#>>> patienceMeasure(overhandUni, 52, 10) 
#(0.50900000000000034, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(overhandBin, 52, 10)   
#(0.49900000000000039, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(doubleOverhand, 52, 10)              
#(0.51800000000000035, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(tripleOverhand, 52, 10)
#(0.47100000000000036, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(mexicanSpiral, 52, 10)
#(0.50100000000000033, {1: [0], 2: [0], 3: [1.0000000000000007], 4: [0], 5: [0], 6: [0], 7: [0], 8: [0], 9: [1.0000000000000007], 10: [0]})
#>>> patienceMeasure(hinduUni, 52, 10)
#(0.48300000000000037, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [1.0000000000000007], 8: [1.0000000000000007], 9: [1.0000000000000007], 10: [1.0000000000000007]})
#>>> patienceMeasure(hinduPois, 52, 10)
#(0.51000000000000034, {1: [1.0000000000000007], 2: [1.0000000000000007], 3: [1.0000000000000007], 4: [1.0000000000000007], 5: [1.0000000000000007], 6: [1.0000000000000007], 7: [0.9970000000000008], 8: [0.9930000000000008], 9: [0.9760000000000008], 10: [0.9660000000000007]})
#>>> patienceMeasure(hinduPois, 52, 10, 3000)
#(0.50766666666667726, {1: [0.9999999999999564], 2: [0.9999999999999564], 3: [0.9999999999999564], 4: [0.9999999999999564], 5: [0.9996666666666231], 6: [0.9986666666666232], 7: [0.9979999999999566], 8: [0.9949999999999569], 9: [0.9769999999999589], 10: [0.9739999999999592]})
#>>> patienceMeasure(hinduPois, 52, 10, 6000)
#(0.49849999999997835, {1: [0.9999999999999232], 2: [0.9999999999999232], 3: [0.9999999999999232], 4: [0.9999999999999232], 5: [0.9999999999999232], 6: [0.9998333333332565], 7: [0.9966666666665902], 8: [0.9943333333332571], 9: [0.9796666666665921], 10: [0.9751666666665926]})    